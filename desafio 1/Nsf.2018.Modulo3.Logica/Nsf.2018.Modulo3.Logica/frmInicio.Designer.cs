﻿namespace Nsf._2018.Modulo3.Logica
{
    partial class frmInicio
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.btnDesafio3 = new System.Windows.Forms.PictureBox();
            this.btnDesafio2 = new System.Windows.Forms.PictureBox();
            this.btnDesafio1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.btnDesafio3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnDesafio2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnDesafio1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Showcard Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.MediumSeaGreen;
            this.label1.Location = new System.Drawing.Point(190, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(123, 30);
            this.label1.TabIndex = 1;
            this.label1.Text = "Desafio 1";
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 12.25F, System.Drawing.FontStyle.Bold);
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(309, 46);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(230, 79);
            this.label2.TabIndex = 2;
            this.label2.Text = "Cálcule quanto será sua próxima viagem!";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 12.25F, System.Drawing.FontStyle.Bold);
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(422, 178);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(179, 79);
            this.label3.TabIndex = 5;
            this.label3.Text = "Reserve seu ingresso para o show do ano!";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Showcard Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.MediumSeaGreen;
            this.label4.Location = new System.Drawing.Point(318, 142);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(124, 30);
            this.label4.TabIndex = 4;
            this.label4.Text = "Desafio 2";
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Segoe UI", 12.25F, System.Drawing.FontStyle.Bold);
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(309, 306);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(196, 79);
            this.label5.TabIndex = 8;
            this.label5.Text = "Conecte seu programa com o do Correio.";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Showcard Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.MediumSeaGreen;
            this.label6.Location = new System.Drawing.Point(199, 278);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(125, 30);
            this.label6.TabIndex = 7;
            this.label6.Text = "Desafio 3";
            // 
            // btnDesafio3
            // 
            this.btnDesafio3.BackColor = System.Drawing.Color.Transparent;
            this.btnDesafio3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnDesafio3.Image = global::Nsf._2018.Modulo3.Logica.Properties.Resources.ex3;
            this.btnDesafio3.Location = new System.Drawing.Point(218, 312);
            this.btnDesafio3.Name = "btnDesafio3";
            this.btnDesafio3.Size = new System.Drawing.Size(83, 86);
            this.btnDesafio3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnDesafio3.TabIndex = 6;
            this.btnDesafio3.TabStop = false;
            this.btnDesafio3.Click += new System.EventHandler(this.btnDesafio3_Click);
            // 
            // btnDesafio2
            // 
            this.btnDesafio2.BackColor = System.Drawing.Color.Transparent;
            this.btnDesafio2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnDesafio2.Image = global::Nsf._2018.Modulo3.Logica.Properties.Resources.ex2;
            this.btnDesafio2.Location = new System.Drawing.Point(339, 175);
            this.btnDesafio2.Name = "btnDesafio2";
            this.btnDesafio2.Size = new System.Drawing.Size(75, 95);
            this.btnDesafio2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnDesafio2.TabIndex = 3;
            this.btnDesafio2.TabStop = false;
            this.btnDesafio2.Click += new System.EventHandler(this.btnDesafio2_Click);
            // 
            // btnDesafio1
            // 
            this.btnDesafio1.BackColor = System.Drawing.Color.Transparent;
            this.btnDesafio1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnDesafio1.Image = global::Nsf._2018.Modulo3.Logica.Properties.Resources.ex1;
            this.btnDesafio1.Location = new System.Drawing.Point(199, 46);
            this.btnDesafio1.Name = "btnDesafio1";
            this.btnDesafio1.Size = new System.Drawing.Size(104, 95);
            this.btnDesafio1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnDesafio1.TabIndex = 0;
            this.btnDesafio1.TabStop = false;
            this.btnDesafio1.Click += new System.EventHandler(this.btnDesafio1_Click);
            // 
            // frmInicio
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(11F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.BackgroundImage = global::Nsf._2018.Modulo3.Logica.Properties.Resources.bg1;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ClientSize = new System.Drawing.Size(603, 416);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.btnDesafio3);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btnDesafio2);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnDesafio1);
            this.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Margin = new System.Windows.Forms.Padding(6);
            this.Name = "frmInicio";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Desafios";
            ((System.ComponentModel.ISupportInitialize)(this.btnDesafio3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnDesafio2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnDesafio1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox btnDesafio1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.PictureBox btnDesafio2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.PictureBox btnDesafio3;
    }
}

