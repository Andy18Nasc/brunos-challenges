﻿using MySql.Data.MySqlClient;
using Nsf._2018.Modulo3.Logica.Telas.DB.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Modulo3.Logica.DB.Desafio2
{
    class IngressoDatabase
    {
        public List<IngressoDTO> Listar()
        {
            string script = @"SELECT * FROM IngressoDB.tb_ingresso";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<IngressoDTO> lista = new List<IngressoDTO>();
            while (reader.Read())
            {
                IngressoDTO dto = new IngressoDTO();
                dto.Id = reader.GetInt32("id_ingresso");
                dto.Nome = reader.GetString("nm_ingresso");
                dto.Data = reader.GetDateTime("dt_ingresso");
                dto.Preco = reader.GetDecimal("vl_preco");
                dto.Tipo = reader.GetString("tp_ingresso");
                dto.Lugar = reader.GetString("ds_lugar");
                dto.Reservado = reader.GetBoolean("bt_reservado");

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }
    }
}
