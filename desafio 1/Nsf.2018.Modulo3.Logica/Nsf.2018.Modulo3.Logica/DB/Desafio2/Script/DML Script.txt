﻿use IngressoDB;

select * from tb_ingresso;

insert into tb_ingresso (nm_ingresso, dt_ingresso, tp_ingresso, ds_lugar, vl_preco, bt_reservado)
	 values ('Falamansa', '2018-10-22', 'Arquibancada', 'A10',    120, false),
            ('Falamansa', '2018-10-22', 'Arquibancada', 'B20',    120, false),
			('Falamansa', '2018-10-22', 'Pista', 		'P100',   80,  false),
			('Falamansa', '2018-10-22', 'Pista', 		'P200',   80,  false),
            ('S.O.A.D.',  '2018-11-15', 'Arquibancada', 'A1010',  300, false),
            ('S.O.A.D.',  '2018-11-15', 'Arquibancada', 'B2020',  300, false),
			('S.O.A.D.',  '2018-11-15', 'Pista', 		'P3030',  200, false),
			('S.O.A.D.',  '2018-11-15', 'Pista', 		'P4040',  200, false);
            
         