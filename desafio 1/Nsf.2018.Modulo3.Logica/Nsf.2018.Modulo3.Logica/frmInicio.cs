﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nsf._2018.Modulo3.Logica
{
    public partial class frmInicio : Form
    {
        public frmInicio()
        {
            InitializeComponent();
        }

        private void btnDesafio1_Click(object sender, EventArgs e)
        {
            Telas.frmDesafio1 tela = new Telas.frmDesafio1();
            tela.ShowDialog();
        }

        private void btnDesafio2_Click(object sender, EventArgs e)
        {
            Telas.frmDesafio2 tela = new Telas.frmDesafio2();
            tela.ShowDialog();
        }

        private void btnDesafio3_Click(object sender, EventArgs e)
        {
            Telas.frmDesafio3 tela = new Telas.frmDesafio3();
            tela.ShowDialog();
        }
    }
}
