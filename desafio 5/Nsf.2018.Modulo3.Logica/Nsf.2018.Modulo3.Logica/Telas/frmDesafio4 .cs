﻿using Nsf._2018.Modulo3.Logica.DB.Desafio1;
using Nsf._2018.Modulo3.Logica.DB.Desafio4;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nsf._2018.Modulo3.Logica.Telas
{
    public partial class frmDesafio4 : Form
    {
        public frmDesafio4()
        {
            InitializeComponent();
        }

        private void btnListar_Click(object sender, EventArgs e)
        {
            FaculdadeBusiness bus = new FaculdadeBusiness();
            List<FaculdadeDTO> lista = bus.Listar();

            dgvFaculdades.AutoGenerateColumns = false;
            dgvFaculdades.DataSource = lista;
        }

        private void btnCalcular_Click(object sender, EventArgs e)
        {
            // Transforma a linha selecionada em DTO
            FaculdadeDTO dto = dgvFaculdades.CurrentRow.DataBoundItem as FaculdadeDTO;

            // Calcula o total de Investimento
            decimal investimento = CalcularInvestimento(dto);

            // Calcula os descontos baseado nas bolsas
            decimal descontos = CalcularBolsas(dto, investimento, chkBolsaA.Checked, chkBolsaB.Checked, chkBolsaC.Checked, chkBolsaD.Checked);

            // Calcula o total
            decimal total = investimento - descontos;

            // Exibe o total no Label
            lblTotal.Text = total.ToString();
        }
    
        private decimal CalcularInvestimento(FaculdadeDTO facul)
        {
            decimal investimento = facul.ValorMensalidade * facul.Semestres * 6;
            return investimento;
        }

        private decimal CalcularBolsas(FaculdadeDTO facul, decimal investimento, bool bolsaA, bool bolsaB, bool bolsaC, bool bolsaD)
        {
            decimal descontos = 0;
            if (bolsaA == true)
            {
                descontos += investimento * (facul.BolsaA / 100);
            }

            if (bolsaB == true)
            {
                descontos += investimento * (facul.BolsaB / 100);
            }

            if (bolsaC == true)
            {
                descontos += investimento * (facul.BolsaC / 100);
            }

            if (bolsaD == true)
            {
                descontos += investimento * (facul.BolsaD / 100);
            }
            return descontos;
        }

    }
}
