﻿using Newtonsoft.Json;
using Nsf._2018.Modulo3.Logica.DB.Desafio3;
using Nsf._2018.Modulo3.Logica.DB.Desafio6;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Windows.Forms;

namespace Nsf._2018.Modulo3.Logica.Telas
{
    public partial class frmDesafio6 : Form
    {
        public frmDesafio6()
        {
            InitializeComponent();
        }
        

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            // Lê e formata a Cidade do textbox
            string cidade = txtCidadeBusca.Text.Trim();

            // Chama função BuscarApiAdvisorCidade
            List<CidadeResponse> resposta = BuscarApiAdvisorCidade(cidade);

            if (resposta.Count > 0)
            {
                // Altera os valores dos textbox com a resposta do advisor
                txtId.Text = resposta[0].id.ToString();
                txtPais.Text = resposta[0].country;
                txtCidade.Text = resposta[0].name;
                txtEstado.Text = resposta[0].state;
            }
        }


        private List<CidadeResponse> BuscarApiAdvisorCidade(string cidade)
        {
            // Cria objeto responsável por conversar com uma API
            WebClient rest = new WebClient();
            rest.Encoding = Encoding.UTF8;

            // Converte a pesquisa em formato URL
            cidade = HttpUtility.UrlEncode(cidade);

            // Chama API do Advisor, concatenando a cidade
            string resposta = rest.DownloadString("http://apiadvisor.climatempo.com.br/api/v1/locale/city?name=" + cidade + "&token=af3bea8ad6576d9b0c3064024edcd746");

            // Transforma a resposta do correio em lista de DTO
            List<CidadeResponse> cidades = JsonConvert.DeserializeObject<List<CidadeResponse>>(resposta);
            return cidades;
        }

        private void btnVerTempo_Click(object sender, EventArgs e)
        {
            TempoResponse tempo = BuscarApiAdivisorTempo(txtId.Text);

            lblTemperatura.Text = tempo.data.temperature.ToString();
            lblHumidade.Text = tempo.data.humidity.ToString();

            if (tempo.data.wind_velocity > 20)
            {
                rdnVinteUmMaior.Checked = true;
            }
            else if (tempo.data.wind_velocity > 10)
            {
                rdnOnzeaVinte.Checked = true;
            }
            else
            {
                rdnZeroaDez.Checked = true;
            }
        }


        private TempoResponse BuscarApiAdivisorTempo(string id)
        {
            // Cria objeto responsável por conversar com uma API
            WebClient rest = new WebClient();
            rest.Encoding = Encoding.UTF8;

            // Converte a pesquisa em formato URL
            id = HttpUtility.UrlEncode(id);

            // Chama API do Advisor, concatenando a cidade
            string resposta = rest.DownloadString("http://apiadvisor.climatempo.com.br/api/v1/weather/locale/" + id + "/current?token=af3bea8ad6576d9b0c3064024edcd746");

            // Transforma a resposta do correio em DTO
            TempoResponse tempo = JsonConvert.DeserializeObject<TempoResponse>(resposta);
            return tempo;
        }
        

    }
}
