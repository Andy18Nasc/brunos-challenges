﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Modulo3.Logica.DB.Desafio6
{
    class TempoData
    {
        public decimal temperature { get; set; }
        public string wind_direction { get; set; }
        public decimal wind_velocity { get; set; }
        public decimal humidity { get; set; }
        public string condition { get; set; }
        public decimal pressure { get; set; }
        public string icon { get; set; }
        public decimal sensation { get; set; }
        public string date { get; set; }

    }
}
