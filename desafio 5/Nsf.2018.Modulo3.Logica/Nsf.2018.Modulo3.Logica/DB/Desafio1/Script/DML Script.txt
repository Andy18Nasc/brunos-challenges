﻿use ViagemDB;


select * from tb_viagem;

insert into tb_viagem (nm_viagem, vl_passagem, vl_hotel_adulto_dia, vl_hotel_crianca_dia)
	 values ('Machu Picchu (PERU)', 	 2000, 100, 50),
		    ('Miame (EUA)', 			 2500, 130, 80),
            ('Jerusálem (ISRAEL)', 		 3500, 120, 90),
            ('Amsterdan (HOLANDA)', 	 4700, 140, 120),
            ('Cairo (EGITO)', 			 4500, 150, 130),
            ('Sidney (AUSTRALIA)', 		 3600, 120, 60),
            ('Paris (FRANÇA)', 			 5000, 170, 60),
            ('Madrid (ESPANHA)', 		 4800, 140, 65),
            ('Porto (PORTUGAL)', 		 4700, 150, 85),
            ('Buenos Aires (ARGENTINA)', 2500, 105, 40);
            
