﻿drop database FaculdadeDB;
create database FaculdadeDB;
use FaculdadeDB;

create table tb_faculdade (
	id_faculdade 		int primary key auto_increment,
    nm_faculdade 		varchar(100),
    vl_mensalidade		decimal(15,2),
    qt_semestres		int,
    vl_bolsaA			decimal(15,2),
    vl_bolsaB			decimal(15,2),
    vl_bolsaC			decimal(15,2),
    vl_bolsaD			decimal(15,2)
);
