﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Modulo3.Logica.DB.Desafio4
{
    class FaculdadeDTO
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public decimal ValorMensalidade { get; set; }
        public int Semestres { get; set; }
        public decimal BolsaA { get; set; }
        public decimal BolsaB { get; set; }
        public decimal BolsaC { get; set; }
        public decimal BolsaD { get; set; }
    }
}
