﻿using MySql.Data.MySqlClient;
using Nsf._2018.Modulo3.Logica.Telas.DB.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Modulo3.Logica.DB.Desafio1
{
    class ViagemDatabase
    {
        public List<ViagemDTO> Listar()
        {
            string script = @"SELECT * FROM tb_viagem";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<ViagemDTO> lista = new List<ViagemDTO>();
            while (reader.Read())
            {
                ViagemDTO dto = new ViagemDTO();
                dto.Id = reader.GetInt32("id_viagem");
                dto.Nome = reader.GetString("nm_viagem");
                dto.ValorPassagemArea = reader.GetDecimal("vl_passagem");
                dto.ValorHotelAdulto= reader.GetDecimal("vl_hotel_adulto_dia");
                dto.ValorHotelCrianca = reader.GetDecimal("vl_hotel_crianca_dia");

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }
        
    }
}
