﻿using MySql.Data.MySqlClient;
using Nsf._2018.Modulo3.Logica.Telas.DB.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Modulo3.Logica.DB.Desafio5
{
    class CursoDatabase
    {
        public List<CursoDTO> Listar()
        {
            string script = @"SELECT * FROM CursoDB.tb_curso";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<CursoDTO> lista = new List<CursoDTO>();
            while (reader.Read())
            {
                CursoDTO dto = new CursoDTO();
                dto.Id = reader.GetInt32("id_curso");
                dto.Nome = reader.GetString("nm_curso");
                dto.QtdHoras = reader.GetInt32("qt_horas");

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }
        
    }
}
