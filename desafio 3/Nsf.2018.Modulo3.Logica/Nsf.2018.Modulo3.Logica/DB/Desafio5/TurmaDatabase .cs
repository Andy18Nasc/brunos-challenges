﻿using MySql.Data.MySqlClient;
using Nsf._2018.Modulo3.Logica.Telas.DB.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Modulo3.Logica.DB.Desafio5
{
    class TurmaDatabase
    {
        public List<CursoDTO> Listar()
        {
            string script = @"SELECT * FROM CursoDB.tb_turma";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<TurmaDTO> lista = new List<TurmaDTO>();
            while (reader.Read())
            {
                TurmaDTO dto = new TurmaDTO();
                dto.Id = reader.GetInt32("id_turma");
                dto.CursoId = reader.GetInt32("id_curso");
                dto.Nome = reader.GetString("nm_curso");
                dto.TotalAlunos = reader.GetInt32("qt_alunos");

                lista.Add(dto);
            }
            reader.Close();
            return lista;
        }
    }
}
