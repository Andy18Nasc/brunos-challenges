﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Modulo3.Logica.DB.Desafio5
{
    class CursoDTO
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public int QtdHoras { get; set; }
    }
}
