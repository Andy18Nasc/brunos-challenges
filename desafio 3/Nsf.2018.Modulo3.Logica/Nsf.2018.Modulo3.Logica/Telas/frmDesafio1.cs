﻿using Nsf._2018.Modulo3.Logica.DB.Desafio1;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nsf._2018.Modulo3.Logica.Telas
{
    public partial class frmDesafio1 : Form
    {
        public frmDesafio1()
        {
            InitializeComponent();
        }

        private void btnListar_Click(object sender, EventArgs e)
        {
            ViagemBusiness bus = new ViagemBusiness();
            List<ViagemDTO> lista = bus.Listar();

            dgvViagem.AutoGenerateColumns = false;
            dgvViagem.DataSource = lista;
        }

        private void btnCalcular_Click(object sender, EventArgs e)
        {
            // Transforma a linha selecionada em DTO
            ViagemDTO dto = dgvViagem.CurrentRow.DataBoundItem as ViagemDTO;

            // Lê os valores dos textbox
            int criancas = Convert.ToInt32(txtCriancas.Text);
            int adultos = Convert.ToInt32(txtAdultos.Text);

            // Lê os valores dos DateTimePicker
            DateTime volta = dtpVolta.Value.Date;
            DateTime ida = dtpIda.Value.Date;
            
            // Chama função que realiza o cálculo
            decimal total = CalcularTotal(dto, adultos, criancas, ida, volta);

            // Exibe o total no Label
            lblTotal.Text = total.ToString();
        }

        private decimal CalcularTotal(ViagemDTO viagemDto, int qtdAdultos, int qtdCriancas, DateTime ida, DateTime volta)
        {
            decimal totalPassagens     = (qtdAdultos + qtdCriancas) * viagemDto.ValorPassagemArea;
            decimal totalHotelAdultos  = qtdAdultos  * viagemDto.ValorHotelAdulto;
            decimal totalHotelCriancas = qtdCriancas * viagemDto.ValorHotelCrianca;

            decimal total = totalPassagens + totalHotelAdultos + totalHotelCriancas;
            return total;
        }


    }
}
