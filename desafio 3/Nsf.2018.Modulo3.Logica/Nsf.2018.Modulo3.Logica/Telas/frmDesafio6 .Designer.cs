﻿namespace Nsf._2018.Modulo3.Logica.Telas
{
    partial class frmDesafio6
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnBuscar = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txtCidadeBusca = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.lblTemperatura = new System.Windows.Forms.Label();
            this.lblHumidade = new System.Windows.Forms.Label();
            this.txtId = new System.Windows.Forms.TextBox();
            this.txtPais = new System.Windows.Forms.TextBox();
            this.txtCidade = new System.Windows.Forms.TextBox();
            this.txtEstado = new System.Windows.Forms.TextBox();
            this.btnVerTempo = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.rdnVinteUmMaior = new System.Windows.Forms.RadioButton();
            this.rdnOnzeaVinte = new System.Windows.Forms.RadioButton();
            this.rdnZeroaDez = new System.Windows.Forms.RadioButton();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnBuscar
            // 
            this.btnBuscar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBuscar.Location = new System.Drawing.Point(335, 29);
            this.btnBuscar.Name = "btnBuscar";
            this.btnBuscar.Size = new System.Drawing.Size(124, 30);
            this.btnBuscar.TabIndex = 1;
            this.btnBuscar.Text = "Buscar";
            this.btnBuscar.UseVisualStyleBackColor = true;
            this.btnBuscar.Click += new System.EventHandler(this.btnBuscar_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 16F);
            this.label1.Location = new System.Drawing.Point(62, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(86, 30);
            this.label1.TabIndex = 2;
            this.label1.Text = "Cidade:";
            // 
            // txtCidadeBusca
            // 
            this.txtCidadeBusca.Location = new System.Drawing.Point(153, 30);
            this.txtCidadeBusca.Name = "txtCidadeBusca";
            this.txtCidadeBusca.Size = new System.Drawing.Size(168, 29);
            this.txtCidadeBusca.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 14F);
            this.label2.Location = new System.Drawing.Point(66, 110);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(81, 25);
            this.label2.TabIndex = 4;
            this.label2.Text = "Local Id:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 14F);
            this.label3.Location = new System.Drawing.Point(98, 215);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(49, 25);
            this.label3.TabIndex = 6;
            this.label3.Text = "País:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 14F);
            this.label4.Location = new System.Drawing.Point(72, 145);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(75, 25);
            this.label4.TabIndex = 8;
            this.label4.Text = "Cidade:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Segoe UI", 14F);
            this.label5.Location = new System.Drawing.Point(75, 180);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(72, 25);
            this.label5.TabIndex = 10;
            this.label5.Text = "Estado:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Segoe UI", 14F);
            this.label6.Location = new System.Drawing.Point(97, 62);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(122, 25);
            this.label6.TabIndex = 12;
            this.label6.Text = "Temperatura:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Segoe UI", 14F);
            this.label7.Location = new System.Drawing.Point(125, 97);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(92, 25);
            this.label7.TabIndex = 13;
            this.label7.Text = "Umidade:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Segoe UI", 14F);
            this.label8.Location = new System.Drawing.Point(29, 131);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(188, 25);
            this.label8.TabIndex = 14;
            this.label8.Text = "Velocidade do vento:";
            // 
            // lblTemperatura
            // 
            this.lblTemperatura.AutoSize = true;
            this.lblTemperatura.Font = new System.Drawing.Font("Segoe UI", 14F);
            this.lblTemperatura.Location = new System.Drawing.Point(225, 62);
            this.lblTemperatura.Name = "lblTemperatura";
            this.lblTemperatura.Size = new System.Drawing.Size(20, 25);
            this.lblTemperatura.TabIndex = 15;
            this.lblTemperatura.Text = "-";
            // 
            // lblHumidade
            // 
            this.lblHumidade.AutoSize = true;
            this.lblHumidade.Font = new System.Drawing.Font("Segoe UI", 14F);
            this.lblHumidade.Location = new System.Drawing.Point(225, 97);
            this.lblHumidade.Name = "lblHumidade";
            this.lblHumidade.Size = new System.Drawing.Size(20, 25);
            this.lblHumidade.TabIndex = 16;
            this.lblHumidade.Text = "-";
            // 
            // txtId
            // 
            this.txtId.Location = new System.Drawing.Point(153, 110);
            this.txtId.Name = "txtId";
            this.txtId.ReadOnly = true;
            this.txtId.Size = new System.Drawing.Size(168, 29);
            this.txtId.TabIndex = 5;
            // 
            // txtPais
            // 
            this.txtPais.Location = new System.Drawing.Point(153, 215);
            this.txtPais.Name = "txtPais";
            this.txtPais.ReadOnly = true;
            this.txtPais.Size = new System.Drawing.Size(168, 29);
            this.txtPais.TabIndex = 7;
            // 
            // txtCidade
            // 
            this.txtCidade.Location = new System.Drawing.Point(153, 145);
            this.txtCidade.Name = "txtCidade";
            this.txtCidade.ReadOnly = true;
            this.txtCidade.Size = new System.Drawing.Size(168, 29);
            this.txtCidade.TabIndex = 9;
            // 
            // txtEstado
            // 
            this.txtEstado.Location = new System.Drawing.Point(153, 180);
            this.txtEstado.Name = "txtEstado";
            this.txtEstado.ReadOnly = true;
            this.txtEstado.Size = new System.Drawing.Size(168, 29);
            this.txtEstado.TabIndex = 11;
            // 
            // btnVerTempo
            // 
            this.btnVerTempo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnVerTempo.Location = new System.Drawing.Point(334, 20);
            this.btnVerTempo.Name = "btnVerTempo";
            this.btnVerTempo.Size = new System.Drawing.Size(124, 30);
            this.btnVerTempo.TabIndex = 18;
            this.btnVerTempo.Text = "Ver o tempo";
            this.btnVerTempo.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtEstado);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.txtCidade);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.txtPais);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txtId);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.txtCidadeBusca);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.btnBuscar);
            this.groupBox1.Location = new System.Drawing.Point(20, 14);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(475, 268);
            this.groupBox1.TabIndex = 19;
            this.groupBox1.TabStop = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.rdnVinteUmMaior);
            this.groupBox2.Controls.Add(this.rdnOnzeaVinte);
            this.groupBox2.Controls.Add(this.rdnZeroaDez);
            this.groupBox2.Controls.Add(this.btnVerTempo);
            this.groupBox2.Controls.Add(this.lblHumidade);
            this.groupBox2.Controls.Add(this.lblTemperatura);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Location = new System.Drawing.Point(20, 288);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(473, 231);
            this.groupBox2.TabIndex = 20;
            this.groupBox2.TabStop = false;
            // 
            // rdnVinteUmMaior
            // 
            this.rdnVinteUmMaior.AutoSize = true;
            this.rdnVinteUmMaior.Location = new System.Drawing.Point(230, 193);
            this.rdnVinteUmMaior.Name = "rdnVinteUmMaior";
            this.rdnVinteUmMaior.Size = new System.Drawing.Size(113, 25);
            this.rdnVinteUmMaior.TabIndex = 21;
            this.rdnVinteUmMaior.TabStop = true;
            this.rdnVinteUmMaior.Text = "21 ou maior";
            this.rdnVinteUmMaior.UseVisualStyleBackColor = true;
            // 
            // rdnOnzeaVinte
            // 
            this.rdnOnzeaVinte.AutoSize = true;
            this.rdnOnzeaVinte.Location = new System.Drawing.Point(230, 162);
            this.rdnOnzeaVinte.Name = "rdnOnzeaVinte";
            this.rdnOnzeaVinte.Size = new System.Drawing.Size(78, 25);
            this.rdnOnzeaVinte.TabIndex = 20;
            this.rdnOnzeaVinte.TabStop = true;
            this.rdnOnzeaVinte.Text = "11 - 20";
            this.rdnOnzeaVinte.UseVisualStyleBackColor = true;
            // 
            // rdnZeroaDez
            // 
            this.rdnZeroaDez.AutoSize = true;
            this.rdnZeroaDez.Location = new System.Drawing.Point(230, 131);
            this.rdnZeroaDez.Name = "rdnZeroaDez";
            this.rdnZeroaDez.Size = new System.Drawing.Size(69, 25);
            this.rdnZeroaDez.TabIndex = 19;
            this.rdnZeroaDez.TabStop = true;
            this.rdnZeroaDez.Text = "0 - 10";
            this.rdnZeroaDez.UseVisualStyleBackColor = true;
            // 
            // frmDesafio6
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ClientSize = new System.Drawing.Size(513, 537);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "frmDesafio6";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Desafio 6";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button btnBuscar;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtCidadeBusca;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label lblTemperatura;
        private System.Windows.Forms.Label lblHumidade;
        private System.Windows.Forms.TextBox txtId;
        private System.Windows.Forms.TextBox txtPais;
        private System.Windows.Forms.TextBox txtCidade;
        private System.Windows.Forms.TextBox txtEstado;
        private System.Windows.Forms.Button btnVerTempo;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton rdnVinteUmMaior;
        private System.Windows.Forms.RadioButton rdnOnzeaVinte;
        private System.Windows.Forms.RadioButton rdnZeroaDez;
    }
}