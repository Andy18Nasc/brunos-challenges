﻿using Nsf._2018.Modulo4.OrientacaoObjeto.ParteII;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nsf._2018.Modulo4.OrientacaoObjeto
{
    public partial class frmCinemaI : Form
    {
        public frmCinemaI()
        {
            InitializeComponent();
        }

        private void btnCalcular_Click(object sender, EventArgs e)
        {
            decimal quant = Convert.ToDecimal(txtQuantidade.Text);
            decimal valor = Convert.ToDecimal(txtValor.Text);

            ingcalc calculo = new ingcalc();

            decimal total = calculo.calcular(quant, valor);

            lblTotal.Text = total.ToString(); 

        }

        private void txtQuantidade_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
