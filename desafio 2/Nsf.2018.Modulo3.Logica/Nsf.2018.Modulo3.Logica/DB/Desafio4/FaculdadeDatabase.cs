﻿using MySql.Data.MySqlClient;
using Nsf._2018.Modulo3.Logica.Telas.DB.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Modulo3.Logica.DB.Desafio4
{
    class FaculdadeDatabase
    {
        public List<FaculdadeDTO> Listar()
        {
            string script = @"SELECT * FROM FaculdadeDB.tb_faculdade";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<FaculdadeDTO> lista = new List<FaculdadeDTO>();
            while (reader.Read())
            {
                FaculdadeDTO dto = new FaculdadeDTO();
                dto.Id = reader.GetInt32("id_faculdade");
                dto.Nome = reader.GetString("nm_faculdade");
                dto.Semestres = reader.GetInt32("qt_semestres");
                dto.ValorMensalidade = reader.GetDecimal("vl_mensalidade");
                dto.BolsaA = reader.GetDecimal("vl_bolsaA");
                dto.BolsaB = reader.GetDecimal("vl_bolsaB");
                dto.BolsaC = reader.GetDecimal("vl_bolsaC");
                dto.BolsaD = reader.GetDecimal("vl_bolsaD");

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }
        
    }
}
