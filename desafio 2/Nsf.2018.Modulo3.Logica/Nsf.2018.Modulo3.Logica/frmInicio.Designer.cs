﻿namespace Nsf._2018.Modulo3.Logica
{
    partial class frmInicio
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.btnDesafio3 = new System.Windows.Forms.PictureBox();
            this.btnDesafio2 = new System.Windows.Forms.PictureBox();
            this.btnDesafio1 = new System.Windows.Forms.PictureBox();
            this.label8 = new System.Windows.Forms.Label();
            this.btnDesafio5 = new System.Windows.Forms.PictureBox();
            this.label10 = new System.Windows.Forms.Label();
            this.btnDesafio4 = new System.Windows.Forms.PictureBox();
            this.label12 = new System.Windows.Forms.Label();
            this.btnDesafio6 = new System.Windows.Forms.PictureBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.btnDesafio3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnDesafio2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnDesafio1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnDesafio5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnDesafio4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnDesafio6)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Showcard Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.MediumSeaGreen;
            this.label1.Location = new System.Drawing.Point(190, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(123, 30);
            this.label1.TabIndex = 1;
            this.label1.Text = "Desafio 1";
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 12.25F, System.Drawing.FontStyle.Bold);
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(309, 46);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(230, 79);
            this.label2.TabIndex = 2;
            this.label2.Text = "Cálcule quanto será sua próxima viagem!";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Showcard Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.MediumSeaGreen;
            this.label4.Location = new System.Drawing.Point(318, 128);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(124, 30);
            this.label4.TabIndex = 4;
            this.label4.Text = "Desafio 2";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Showcard Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.MediumSeaGreen;
            this.label6.Location = new System.Drawing.Point(199, 247);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(125, 30);
            this.label6.TabIndex = 7;
            this.label6.Text = "Desafio 3";
            // 
            // btnDesafio3
            // 
            this.btnDesafio3.BackColor = System.Drawing.Color.Transparent;
            this.btnDesafio3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnDesafio3.Image = global::Nsf._2018.Modulo3.Logica.Properties.Resources.ex3;
            this.btnDesafio3.Location = new System.Drawing.Point(218, 281);
            this.btnDesafio3.Name = "btnDesafio3";
            this.btnDesafio3.Size = new System.Drawing.Size(83, 86);
            this.btnDesafio3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnDesafio3.TabIndex = 6;
            this.btnDesafio3.TabStop = false;
            this.btnDesafio3.Click += new System.EventHandler(this.btnDesafio3_Click);
            // 
            // btnDesafio2
            // 
            this.btnDesafio2.BackColor = System.Drawing.Color.Transparent;
            this.btnDesafio2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnDesafio2.Image = global::Nsf._2018.Modulo3.Logica.Properties.Resources.ex2;
            this.btnDesafio2.Location = new System.Drawing.Point(339, 161);
            this.btnDesafio2.Name = "btnDesafio2";
            this.btnDesafio2.Size = new System.Drawing.Size(75, 95);
            this.btnDesafio2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnDesafio2.TabIndex = 3;
            this.btnDesafio2.TabStop = false;
            this.btnDesafio2.Click += new System.EventHandler(this.btnDesafio2_Click);
            // 
            // btnDesafio1
            // 
            this.btnDesafio1.BackColor = System.Drawing.Color.Transparent;
            this.btnDesafio1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnDesafio1.Image = global::Nsf._2018.Modulo3.Logica.Properties.Resources.ex1;
            this.btnDesafio1.Location = new System.Drawing.Point(199, 46);
            this.btnDesafio1.Name = "btnDesafio1";
            this.btnDesafio1.Size = new System.Drawing.Size(104, 95);
            this.btnDesafio1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnDesafio1.TabIndex = 0;
            this.btnDesafio1.TabStop = false;
            this.btnDesafio1.Click += new System.EventHandler(this.btnDesafio1_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Showcard Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.DarkKhaki;
            this.label8.Location = new System.Drawing.Point(209, 494);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(125, 30);
            this.label8.TabIndex = 16;
            this.label8.Text = "Desafio 5";
            // 
            // btnDesafio5
            // 
            this.btnDesafio5.BackColor = System.Drawing.Color.Transparent;
            this.btnDesafio5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnDesafio5.Image = global::Nsf._2018.Modulo3.Logica.Properties.Resources.des5;
            this.btnDesafio5.Location = new System.Drawing.Point(228, 531);
            this.btnDesafio5.Name = "btnDesafio5";
            this.btnDesafio5.Size = new System.Drawing.Size(83, 86);
            this.btnDesafio5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnDesafio5.TabIndex = 15;
            this.btnDesafio5.TabStop = false;
            this.btnDesafio5.Click += new System.EventHandler(this.btnDesafio5_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Showcard Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.label10.Location = new System.Drawing.Point(318, 365);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(127, 30);
            this.label10.TabIndex = 13;
            this.label10.Text = "Desafio 4";
            // 
            // btnDesafio4
            // 
            this.btnDesafio4.BackColor = System.Drawing.Color.Transparent;
            this.btnDesafio4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnDesafio4.Image = global::Nsf._2018.Modulo3.Logica.Properties.Resources.des4;
            this.btnDesafio4.Location = new System.Drawing.Point(323, 398);
            this.btnDesafio4.Name = "btnDesafio4";
            this.btnDesafio4.Size = new System.Drawing.Size(91, 95);
            this.btnDesafio4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnDesafio4.TabIndex = 12;
            this.btnDesafio4.TabStop = false;
            this.btnDesafio4.Click += new System.EventHandler(this.btnDesafio4_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Showcard Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.SteelBlue;
            this.label12.Location = new System.Drawing.Point(318, 618);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(126, 30);
            this.label12.TabIndex = 19;
            this.label12.Text = "Desafio 6";
            // 
            // btnDesafio6
            // 
            this.btnDesafio6.BackColor = System.Drawing.Color.Transparent;
            this.btnDesafio6.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnDesafio6.Image = global::Nsf._2018.Modulo3.Logica.Properties.Resources.des6;
            this.btnDesafio6.Location = new System.Drawing.Point(323, 651);
            this.btnDesafio6.Name = "btnDesafio6";
            this.btnDesafio6.Size = new System.Drawing.Size(91, 95);
            this.btnDesafio6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnDesafio6.TabIndex = 18;
            this.btnDesafio6.TabStop = false;
            this.btnDesafio6.Click += new System.EventHandler(this.btnDesafio6_Click);
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 12.25F, System.Drawing.FontStyle.Bold);
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(422, 164);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(179, 79);
            this.label3.TabIndex = 5;
            this.label3.Text = "Reserve seu ingresso para o show do ano!";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Segoe UI", 12.25F, System.Drawing.FontStyle.Bold);
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(309, 275);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(196, 79);
            this.label5.TabIndex = 8;
            this.label5.Text = "Conecte seu programa com o do Correio.";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label7
            // 
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Segoe UI", 12.25F, System.Drawing.FontStyle.Bold);
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(319, 522);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(220, 79);
            this.label7.TabIndex = 17;
            this.label7.Text = "Veja como estão as turmas dos cursos atuais.";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label11
            // 
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Segoe UI", 12.25F, System.Drawing.FontStyle.Bold);
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(422, 654);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(179, 79);
            this.label11.TabIndex = 20;
            this.label11.Text = "Conecte seu programa com a previsão de tempo.";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label9
            // 
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Segoe UI", 12.25F, System.Drawing.FontStyle.Bold);
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(422, 401);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(179, 79);
            this.label9.TabIndex = 14;
            this.label9.Text = "Calcule quanto será o investimento de sua faculdade.";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // frmInicio
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(11F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImage = global::Nsf._2018.Modulo3.Logica.Properties.Resources.bg1;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ClientSize = new System.Drawing.Size(603, 758);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.btnDesafio6);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.btnDesafio5);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.btnDesafio4);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.btnDesafio3);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btnDesafio2);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnDesafio1);
            this.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Margin = new System.Windows.Forms.Padding(6);
            this.Name = "frmInicio";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Desafios";
            ((System.ComponentModel.ISupportInitialize)(this.btnDesafio3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnDesafio2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnDesafio1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnDesafio5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnDesafio4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnDesafio6)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox btnDesafio1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.PictureBox btnDesafio2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.PictureBox btnDesafio3;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.PictureBox btnDesafio5;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.PictureBox btnDesafio4;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.PictureBox btnDesafio6;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label9;
    }
}

