﻿namespace Nsf._2018.Modulo3.Logica.Telas
{
    partial class frmDesafio4
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dgvFaculdades = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnListar = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.chkBolsaD = new System.Windows.Forms.CheckBox();
            this.chkBolsaC = new System.Windows.Forms.CheckBox();
            this.chkBolsaB = new System.Windows.Forms.CheckBox();
            this.chkBolsaA = new System.Windows.Forms.CheckBox();
            this.btnCalcular = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lblTotal = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.dgvFaculdades)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgvFaculdades
            // 
            this.dgvFaculdades.AllowUserToAddRows = false;
            this.dgvFaculdades.AllowUserToDeleteRows = false;
            this.dgvFaculdades.ColumnHeadersHeight = 40;
            this.dgvFaculdades.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvFaculdades.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column7,
            this.Column2,
            this.Column3,
            this.Column4,
            this.Column5,
            this.Column6});
            this.dgvFaculdades.Location = new System.Drawing.Point(12, 63);
            this.dgvFaculdades.Name = "dgvFaculdades";
            this.dgvFaculdades.ReadOnly = true;
            this.dgvFaculdades.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvFaculdades.RowTemplate.Height = 30;
            this.dgvFaculdades.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvFaculdades.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvFaculdades.Size = new System.Drawing.Size(606, 195);
            this.dgvFaculdades.TabIndex = 0;
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "Nome";
            this.Column1.HeaderText = "Faculdade";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 250;
            // 
            // Column7
            // 
            this.Column7.DataPropertyName = "ValorMensalidade";
            this.Column7.HeaderText = "Valor Mensalidade";
            this.Column7.Name = "Column7";
            this.Column7.ReadOnly = true;
            this.Column7.Width = 150;
            // 
            // Column2
            // 
            this.Column2.DataPropertyName = "Semestres";
            this.Column2.HeaderText = "Semestres";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            // 
            // Column3
            // 
            this.Column3.DataPropertyName = "BolsaA";
            this.Column3.HeaderText = "Bolsa A (%)";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            // 
            // Column4
            // 
            this.Column4.DataPropertyName = "BolsaB";
            this.Column4.HeaderText = "Bolsa B (%)";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            // 
            // Column5
            // 
            this.Column5.DataPropertyName = "BolsaC";
            this.Column5.HeaderText = "Bolsa C (%)";
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            // 
            // Column6
            // 
            this.Column6.DataPropertyName = "BolsaD";
            this.Column6.HeaderText = "Bolsa D (%)";
            this.Column6.Name = "Column6";
            this.Column6.ReadOnly = true;
            // 
            // btnListar
            // 
            this.btnListar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnListar.Location = new System.Drawing.Point(12, 23);
            this.btnListar.Name = "btnListar";
            this.btnListar.Size = new System.Drawing.Size(152, 34);
            this.btnListar.TabIndex = 1;
            this.btnListar.Text = "Listar Faculdades";
            this.btnListar.UseVisualStyleBackColor = true;
            this.btnListar.Click += new System.EventHandler(this.btnListar_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.chkBolsaD);
            this.groupBox1.Controls.Add(this.chkBolsaC);
            this.groupBox1.Controls.Add(this.chkBolsaB);
            this.groupBox1.Controls.Add(this.chkBolsaA);
            this.groupBox1.Location = new System.Drawing.Point(12, 266);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(293, 180);
            this.groupBox1.TabIndex = 11;
            this.groupBox1.TabStop = false;
            // 
            // chkBolsaD
            // 
            this.chkBolsaD.AutoSize = true;
            this.chkBolsaD.Location = new System.Drawing.Point(163, 115);
            this.chkBolsaD.Name = "chkBolsaD";
            this.chkBolsaD.Size = new System.Drawing.Size(81, 25);
            this.chkBolsaD.TabIndex = 3;
            this.chkBolsaD.Text = "Bolsa D";
            this.chkBolsaD.UseVisualStyleBackColor = true;
            // 
            // chkBolsaC
            // 
            this.chkBolsaC.AutoSize = true;
            this.chkBolsaC.Location = new System.Drawing.Point(162, 52);
            this.chkBolsaC.Name = "chkBolsaC";
            this.chkBolsaC.Size = new System.Drawing.Size(80, 25);
            this.chkBolsaC.TabIndex = 2;
            this.chkBolsaC.Text = "Bolsa C";
            this.chkBolsaC.UseVisualStyleBackColor = true;
            // 
            // chkBolsaB
            // 
            this.chkBolsaB.AutoSize = true;
            this.chkBolsaB.Location = new System.Drawing.Point(45, 117);
            this.chkBolsaB.Name = "chkBolsaB";
            this.chkBolsaB.Size = new System.Drawing.Size(79, 25);
            this.chkBolsaB.TabIndex = 1;
            this.chkBolsaB.Text = "Bolsa B";
            this.chkBolsaB.UseVisualStyleBackColor = true;
            // 
            // chkBolsaA
            // 
            this.chkBolsaA.AutoSize = true;
            this.chkBolsaA.Location = new System.Drawing.Point(44, 52);
            this.chkBolsaA.Name = "chkBolsaA";
            this.chkBolsaA.Size = new System.Drawing.Size(80, 25);
            this.chkBolsaA.TabIndex = 0;
            this.chkBolsaA.Text = "Bolsa A";
            this.chkBolsaA.UseVisualStyleBackColor = true;
            // 
            // btnCalcular
            // 
            this.btnCalcular.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCalcular.Location = new System.Drawing.Point(12, 458);
            this.btnCalcular.Name = "btnCalcular";
            this.btnCalcular.Size = new System.Drawing.Size(293, 34);
            this.btnCalcular.TabIndex = 12;
            this.btnCalcular.Text = "Calcular Total do Curso";
            this.btnCalcular.UseVisualStyleBackColor = true;
            this.btnCalcular.Click += new System.EventHandler(this.btnCalcular_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Segoe UI", 24F, System.Drawing.FontStyle.Bold);
            this.label5.Location = new System.Drawing.Point(393, 324);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(195, 45);
            this.label5.TabIndex = 11;
            this.label5.Text = "Valor Total:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Segoe UI", 20F);
            this.label6.Location = new System.Drawing.Point(394, 369);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(48, 37);
            this.label6.TabIndex = 13;
            this.label6.Text = "R$";
            // 
            // lblTotal
            // 
            this.lblTotal.AutoSize = true;
            this.lblTotal.Font = new System.Drawing.Font("Segoe UI", 20F);
            this.lblTotal.Location = new System.Drawing.Point(448, 369);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(68, 37);
            this.lblTotal.TabIndex = 14;
            this.lblTotal.Text = "0,00";
            this.lblTotal.Click += new System.EventHandler(this.lblTotal_Click);
            // 
            // toolTip1
            // 
            this.toolTip1.AutoPopDelay = 10000;
            this.toolTip1.InitialDelay = 500;
            this.toolTip1.ReshowDelay = 100;
            this.toolTip1.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.toolTip1.ToolTipTitle = "DICA";
            // 
            // frmDesafio4
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ClientSize = new System.Drawing.Size(630, 506);
            this.Controls.Add(this.lblTotal);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.btnCalcular);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnListar);
            this.Controls.Add(this.dgvFaculdades);
            this.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "frmDesafio4";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Desafio 4";
            ((System.ComponentModel.ISupportInitialize)(this.dgvFaculdades)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvFaculdades;
        private System.Windows.Forms.Button btnListar;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnCalcular;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblTotal;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.CheckBox chkBolsaD;
        private System.Windows.Forms.CheckBox chkBolsaC;
        private System.Windows.Forms.CheckBox chkBolsaB;
        private System.Windows.Forms.CheckBox chkBolsaA;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
    }
}