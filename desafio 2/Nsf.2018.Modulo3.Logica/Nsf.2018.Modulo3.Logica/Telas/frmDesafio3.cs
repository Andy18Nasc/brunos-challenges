﻿using Newtonsoft.Json;
using Nsf._2018.Modulo3.Logica.DB.Desafio3;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nsf._2018.Modulo3.Logica.Telas
{
    public partial class frmDesafio3 : Form
    {
        public frmDesafio3()
        {
            InitializeComponent();
        }


        private void btnBuscar_Click(object sender, EventArgs e)
        {
            // Lê e formata o CEP do textbox
            string cep = txtCEP.Text.Trim().Replace("-", "");

            // Chama função BuscarAPICorreio
            CorreioResponse correio = BuscarAPICorreio(cep);

            // Altera os valores dos textbox com a resposta do correio
            txtLogradouro.Text = correio.logradouro + " - " + correio.complemento;
            txtBairro.Text = correio.bairro;
            txtCidade.Text = correio.localidade;
            txtEstado.Text = correio.uf;
        }


        private CorreioResponse BuscarAPICorreio(string cep)
        {
            // Cria objeto responsável por conversar com uma API
            WebClient rest = new WebClient();
            rest.Encoding = Encoding.UTF8;

            // Chama API do correio, concatenando o cep
            string resposta = rest.DownloadString("https://viacep.com.br/ws/" + cep + "/json");

            // Transforma a resposta do correio em DTO
            CorreioResponse correio = JsonConvert.DeserializeObject<CorreioResponse>(resposta);
            return correio;
        }

        private void frmDesafio3_Load(object sender, EventArgs e)
        {

        }
    }
}
