﻿using Nsf._2018.Modulo3.Logica.DB.Desafio1;
using Nsf._2018.Modulo3.Logica.DB.Desafio4;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nsf._2018.Modulo3.Logica.Telas
{
    public partial class frmDesafio4 : Form
    {
        public frmDesafio4()
        {
            InitializeComponent();
        }

        private void btnListar_Click(object sender, EventArgs e)
        {
            FaculdadeBusiness bus = new FaculdadeBusiness();
            List<FaculdadeDTO> lista = bus.Listar();

            dgvFaculdades.AutoGenerateColumns = false;
            dgvFaculdades.DataSource = lista;
        }

        private void btnCalcular_Click(object sender, EventArgs e)
        {

        }

        private void lblTotal_Click(object sender, EventArgs e)
        {

        }
    }
}
