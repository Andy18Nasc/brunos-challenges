﻿using Nsf._2018.Modulo3.Logica.DB.Desafio1;
using Nsf._2018.Modulo3.Logica.DB.Desafio5;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nsf._2018.Modulo3.Logica.Telas
{
    public partial class frmDesafio5 : Form
    {
        public frmDesafio5()
        {
            InitializeComponent();
            CarregarCursos();
        }

        private void CarregarCursos()
        {
            CursoBusiness bus = new CursoBusiness();
            List<CursoDTO> lista = bus.Listar();

            cboCurso.ValueMember = nameof(CursoDTO.Id);
            cboCurso.DisplayMember = nameof(CursoDTO.Nome);
            cboCurso.DataSource = lista;
        }

        private void frmDesafio5_Load(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }
    }
}
